# Docker環境構築 php(laravel) nginx mysql

## 自分用Docker環境構築メモ
## git clone
任意の作業ディレクトリ（ここではPHPApp）を作成し、Dockerの設定ファイルを `git clone` する。

```
$ mkdir PHPApp
$ cd PHPApp
$ git clone https://ta_furuya@bitbucket.org/ta_furuya/docker_phpapplaravel.git
$ cd docker_phpapplaravel
```

## nginx.confの適用
`/nginx` ディレクトリの `example_nginx.conf` を `nginx.conf` にリネーム。

## buildとdocker起動
docker-composeコマンドにてbuildおよび起動を行う。

```
$ docker-compose build
$ docker-compose up -d
```

## laravel設定
composerコマンドでlaravelプロジェクト（ここではlaravel_app）を作成する。

```
$ docker-compose exec php bash
root@xxxxxxxxxxxx:/var/www/html# composer create-project laravel/laravel laravel_app --prefer-dist
root@xxxxxxxxxxxx:/var/www/html# exit
```

MySQLを利用ためにとりあえずDBを作成しておく。
※mysqlのrootのパスワードはdocker-compose.ymlに記載してある`root`

```
$ docker-compose exec mysql bash
root@xxxxxxxxxxxx:/# mysql -u root -p
mysql> create database php_app_db;
mysql> exit;
root@xxxxxxxxxxxx:/# exit;
```

## Docker設定の調整
`./nginx/nginx.conf`

```
root /var/www/html/;
```
を
```
root /var/www/html/laravel_app/public;
```
に変更。

`./www/html/laravel_app/.env`
mysql関連の設定を下記に変更。

```
DB_CONNECTION=mysql
DB_HOST=docker_mysql
DB_PORT=3306
DB_DATABASE=php_app_db
DB_USERNAME=root
DB_PASSWORD=root
```

## Dockerの再起動
```
$ docker-compose down
$ docker-compose build
$ docker-compose up -d
```
## laravelコマンド類
テストやartisanコマンドについては `docker-compose exec php bash` によりphpサービス内から実行を行う。
